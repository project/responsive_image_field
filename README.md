# Responsive Image Field

---------------------
### Content of this file

* Introduction
* Requirements
* Installation
* Supporting organization

------------
## Introduction

**Responsive Image Field** provides new field that allows the implementation of responsive images with the possibility of replacing the source image for individual breakpoints.

This module is based on the core module **Responsive Image** and extends it's functionality by adding the possibility of replacing the source image for individual breakpoints.

------------
## Requirements

* [Media Library Form API Element](https://www.drupal.org/project/media_library_form_element) - for selecting images via media library widget.

------------

## Installation

Install the Responsive Image Field module as you would normally install a
contributed Drupal module. Visit [Installing Modules](https://www.drupal.org/node/1897420)
for further information.

------------
## Supporting organization:

* [Smartbees](https://www.drupal.org/smartbees)
