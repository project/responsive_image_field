<?php

declare(strict_types=1);

namespace Drupal\responsive_image_field\Plugin\Field\FieldFormatter;

use Drupal\breakpoint\BreakpointManagerInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\file\FileInterface;
use Drupal\media\MediaInterface;
use Drupal\responsive_image\ResponsiveImageStyleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the "Responsive Image Field" default formatter.
 *
 * @FieldFormatter(
 *   id = "responsive_image_field_formatter",
 *   label = @Translation("Responsive Image Field"),
 *   field_types = {
 *     "responsive_image_field",
 *   }
 * )
 */
class ResponsiveImageFieldDefaultFormatter extends FormatterBase {

  /**
   * Constructs a ResponsiveImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\breakpoint\BreakpointManagerInterface $breakpointManager
   *   The breakpoint manager service.
   */
  public function __construct(
    string $plugin_id,
    array $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    string $label,
    string $view_mode,
    array $third_party_settings,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected BreakpointManagerInterface $breakpointManager,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ): static {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('breakpoint.manager'),
    );
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore-next-line
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $element = [];
    $responsiveImageStyleStorage = $this->entityTypeManager->getStorage('responsive_image_style');
    $imageStyleStorage = $this->entityTypeManager->getStorage('image_style');
    $mediaStorage = $this->entityTypeManager->getStorage('media');

    // Collect cache tags to be added for each item in the field.
    $responsive_image_style = $responsiveImageStyleStorage->load($this->getFieldSetting('responsive_image_style'));
    if (!$responsive_image_style instanceof ResponsiveImageStyleInterface) {
      return $element;
    }

    $cache_tags = [];

    $cache_tags = Cache::mergeTags($cache_tags, $responsive_image_style->getCacheTags());
    $image_styles_to_load = $responsive_image_style->getImageStyleIds();
    $image_styles = $imageStyleStorage->loadMultiple($image_styles_to_load);
    foreach ($image_styles as $image_style) {
      $cache_tags = Cache::mergeTags($cache_tags, $image_style->getCacheTags());
    }

    // Load saved field data.
    foreach ($items as $delta => $item) {
      $values = $item->getValue();
      if (!isset($values) || !isset($values['target_id'])) {
        continue;
      }
      $image = $mediaStorage->load($values['target_id']);
      if (!$image instanceof MediaInterface) {
        continue;
      }
      $attributes = $image->get('field_media_image')->getValue()[0] ?? [];
      $file = $image->get('field_media_image')->entity;

      if (!$file instanceof FileInterface) {
        continue;
      }

      $sources = [];
      $configuredBreakpoints = [];
      if (isset($values['multiple_target_ids']) && $values['multiple_target_ids']) {
        $configuredBreakpoints = json_decode($values['multiple_target_ids'], TRUE);
        if (json_last_error() !== JSON_ERROR_NONE) {
          $configuredBreakpoints = [];
        }
      }

      /*
       * Retrieve all breakpoints and multipliers and reverse order of
       * breakpoints. By default, breakpoints are ordered from smallest
       * weight to largest: the smallest weight is expected to have the
       * smallest breakpoint width, while the largest weight is expected
       * to have the largest breakpoint width. For responsive images,
       * we need the largest breakpoint widths first, so we need to reverse
       * the order of these breakpoints.
       */
      $breakpoints = array_reverse($this->breakpointManager->getBreakpointsByGroup($responsive_image_style->getBreakpointGroup()));
      foreach ($responsive_image_style->getKeyedImageStyleMappings() as $breakpoint_id => $multipliers) {
        if (!isset($breakpoints[$breakpoint_id])) {
          continue;
        }

        // Get Image URI for default image.
        $imageUri = $file->getFileUri();
        // Fix field name bug that broke media_library_form_element module.
        $clearedBreakpointId = preg_replace('/\W+/', '_', $breakpoint_id);

        // Replace Image URI for one defined in the field configuration.
        if (isset($configuredBreakpoints[$clearedBreakpointId])) {
          $changedImage = $mediaStorage->load($configuredBreakpoints[$clearedBreakpointId]);
          if (!$changedImage instanceof MediaInterface) {
            continue;
          }

          $changedImageFile = $changedImage->get('field_media_image')->entity;
          if (!$changedImageFile instanceof FileInterface) {
            continue;
          }

          $imageUri = $changedImageFile->getFileUri();
        }

        // Render source array attributes for provided image.
        $sources[] = _responsive_image_build_source_attributes(['uri' => $imageUri], $breakpoints[$breakpoint_id], $multipliers);
      }

      // There is only one source tag with an empty media attribute. This means
      // we can output an image tag with the srcset attribute instead of a
      // picture tag.
      $element[$delta] = [
        '#theme' => 'responsive_image_field',
        '#default_image' => [
          '#theme' => 'image',
          '#uri' => _responsive_image_image_style_url($responsive_image_style->getFallbackImageStyle(), $file->getFileUri()),
          '#width' => $attributes['width'] ?? NULL,
          '#height' => $attributes['height'] ?? NULL,
          '#alt' => $attributes['alt'] ?? '',
          '#title' => $attributes['title'] ?? '',
        ],
        '#output_image' => count($sources) == 0,
        '#sources' => $sources,
        '#cache' => [
          'tags' => $cache_tags,
        ],
      ];
    }
    return $element;
  }

}
