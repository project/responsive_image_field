<?php

declare(strict_types=1);

namespace Drupal\responsive_image_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type for Responsive Image Field.
 *
 * @FieldType(
 *   id = "responsive_image_field",
 *   label = @Translation("Responsive Image Field"),
 *   description = @Translation("Field to use different images for individual responsive image style breakpoints."),
 *   default_formatter = "responsive_image_field_formatter",
 *   default_widget = "responsive_image_field_widget",
 * )
 */
class ResponsiveImageFieldItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    return [
      'columns' => [
        'target_id' => [
          'description' => 'The ID of the media entity',
          'type' => 'int',
          'size' => 'normal',
          'unsigned' => TRUE,
        ],
        'multiple_target_ids' => [
          'description' => 'The ID of the media entities for breakpoints media replacement',
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties = [];

    $properties['target_id'] = DataDefinition::create('integer')
      ->setLabel(t('Media ID'))
      ->setRequired(FALSE);

    $properties['multiple_target_ids'] = DataDefinition::create('string')
      ->setLabel(t('Breakpoints media replacements configuration'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings(): array {
    return [
      'responsive_image_style' => '',
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array {
    $element = [];

    $element['responsive_image_style'] = [
      '#title' => $this->t('Responsive image style'),
      '#type' => 'select',
      '#options' => $this->loadResponsiveImageStyles(),
      '#default_value' => $this->getSetting('responsive_image_style'),
      '#ajax' => [
        'callback' => [$this, 'ajaxRebuildForm'],
        'wrapper' => 'field-combined',
        'event' => 'change',
        'progress' => ['type' => 'throbber'],
      ],
    ];

    return $element;
  }

  /**
   * Loads all responsive image styles and returns them .
   *
   * @return array
   *   An associative array containing responsive image styles, where the key
   *   is the machine name of the style and the value is the label of the style.
   */
  private function loadResponsiveImageStyles(): array {
    $options = [];
    $responsiveImageStorage = \Drupal::entityTypeManager()->getStorage('responsive_image_style');
    $responsiveImageStyles = $responsiveImageStorage->loadMultiple();
    foreach ($responsiveImageStyles as $key => $item) {
      $options[$key] = $item->label();
    }
    return $options;
  }

  /**
   * Reload breakpoints for selected responsive image style.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   Rebuilt form.
   */
  public function ajaxRebuildForm(array &$form, FormStateInterface $form_state): array {
    $form_state->setRebuild();
    return $form;
  }

}
