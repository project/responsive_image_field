<?php

declare(strict_types=1);

namespace Drupal\responsive_image_field\Plugin\Field\FieldWidget;

use Drupal\breakpoint\BreakpointManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\image\ImageStyleInterface;
use Drupal\responsive_image\ResponsiveImageStyleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Responsive Image Field widget.
 *
 * @FieldWidget(
 *   id = "responsive_image_field_widget",
 *   label = @Translation("Responsive Image Field"),
 *   field_types = {
 *     "responsive_image_field",
 *   }
 * )
 */
class ResponsiveImageFieldWidget extends WidgetBase {

  /**
   * Responsive image style machine name.
   */
  protected string $responsiveImageStyle;

  /**
   * Image styles effects information array.
   */
  protected array $imageStylesEffects = [];

  /**
   * Constructs a WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\breakpoint\BreakpointManagerInterface $breakpointManager
   *   The breakpoint manager.
   */
  public function __construct(
    string $plugin_id,
    array $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected BreakpointManagerInterface $breakpointManager,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->responsiveImageStyle = $this->getFieldSetting('responsive_image_style');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ): static {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('breakpoint.manager')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore-next-line
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state,
  ): array {

    // Handle AJAX form re-loading in field configuration.
    $formValues = $form_state->getValues();
    if (isset($formValues['settings']['responsive_image_style']) && $formValues['settings']['responsive_image_style']) {
      $this->responsiveImageStyle = $formValues['settings']['responsive_image_style'];
    }

    $responsiveImageStyleStorage = $this->entityTypeManager->getStorage('responsive_image_style');
    $responsiveStyle = $responsiveImageStyleStorage->load($this->responsiveImageStyle);
    if (!$responsiveStyle instanceof ResponsiveImageStyleInterface) {
      return $element;
    }

    // Load image styles effects applied for the current responsive image style.
    $this->loadImageStyleEffectsInfo($responsiveStyle);

    // Load default / saved values.
    $values = isset($items[$delta]) ? $items[$delta]->getValue() : [];
    if (count($values) > 0) {
      $breakpointsConfiguration = json_decode($values['multiple_target_ids'], TRUE);
      if (json_last_error() !== JSON_ERROR_NONE) {
        $breakpointsConfiguration = [];
      }
    }

    $element += [
      '#type' => 'details',
      '#open' => 'TRUE',
    ];

    $element['default_image'] = [
      '#type' => 'media_library',
      '#allowed_bundles' => ['image'],
      '#default_value' => $values['target_id'] ?? '',
      '#title' => $this->t('Upload default image'),
    ];

    // Get only available breakpoints for image replacement.
    $availableBreakpoints = [];
    $breakpoints = $this->breakpointManager->getBreakpointsByGroup($responsiveStyle->getBreakpointGroup());
    $breakpointMapping = $responsiveStyle->getKeyedImageStyleMappings();
    foreach ($breakpointMapping as $breakpoint_id => $multipliers) {
      if (isset($breakpoints[$breakpoint_id])) {
        $availableBreakpoints[$breakpoint_id] = $breakpoints[$breakpoint_id];
      }
    }

    if (!isset($availableBreakpoints)) {
      // If there are no available breakpoints for image replacement show a message.
      $element['responsive_images_info'] = [
        '#label' => 'item',
        '#markup' => $this->t("In order to change image for different breakpoints, you need to enable image styles <a href='@url'>here</a>", [
          '@url' => Url::fromRoute('entity.responsive_image_style.edit_form', [
            'responsive_image_style' => $this->responsiveImageStyle,
          ])->toString(),
        ]),
      ];

      return $element;
    }

    $element['responsive_images'] = [
      '#type' => 'details',
      '#title' => $this->t('Modify image on breakpoints'),
    ];

    foreach ($availableBreakpoints as $key => $breakpoint) {
      // Fix field name bug that broke media_library_form_element module.
      $fixed_key = preg_replace('/\W+/', '_', $key);

      $element['responsive_images'][$fixed_key] = [
        '#type' => 'media_library',
        '#allowed_bundles' => ['image'],
        '#default_value' => $breakpointsConfiguration[$fixed_key] ?? '',
        '#title' => $this->t("Replace image for <strong> @label</strong> breakpoint", ['@label' => $breakpoint->getLabel()]),
      ];

      // Generate breakpoints information about applied image styles.
      $element['responsive_images'][$fixed_key]['more_info'] = $this->generateBreakpointInfo($breakpointMapping[$key]);
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state): array {
    foreach ($values as &$value) {
      if (!$value) {
        continue;
      }

      if (array_key_exists('default_image', $value)) {
        $value['target_id'] = $value['default_image'];
      }

      $value['multiple_target_ids'] = [];
      if (array_key_exists('responsive_images', $value)) {
        foreach ($value['responsive_images'] as $key => $responsiveImage) {
          if ($responsiveImage) {
            $value['multiple_target_ids'][$key] = $responsiveImage;
          }
        }
      }
      $value['multiple_target_ids'] = json_encode($value['multiple_target_ids']);
    }

    return $values;
  }

  /**
   * Loads and stores information about the image styles and their effects.
   *
   * @param \Drupal\responsive_image\ResponsiveImageStyleInterface $responsive_style
   *   The responsive image style for which to load and store information.
   */
  private function loadImageStyleEffectsInfo(ResponsiveImageStyleInterface $responsive_style): void {
    $imageStyleStorage = $this->entityTypeManager->getStorage('image_style');
    $imageStyles = $responsive_style->getImageStyleIds();
    $imageStylesEffects = [];
    foreach ($imageStyles as $image) {
      $imageStyle = $imageStyleStorage->load($image);
      if (!$imageStyle instanceof ImageStyleInterface) {
        continue;
      }
      $imageStylesEffects[$image] = [];
      $effects = $imageStyle->getEffects();
      foreach ($effects as $effect) {
        $imageStylesEffects[$image][] = [
          'title' => $imageStyle->label(),
          'label' => $effect->label(),
          'summary' => $effect->getSummary(),
        ];
      }
    }
    $this->imageStylesEffects = $imageStylesEffects;
  }

  /**
   * Render breakpoint information about applied image_styles.
   *
   * @param array $breakpointConfiguration
   *   The current breakpoints' configuration.
   *
   * @return array
   *   Rendered breakpoint information.
   */
  private function generateBreakpointInfo(array $breakpointConfiguration): array {
    $template = [
      '#type' => 'details',
      '#weight' => 10,
      '#title' => $this->t('Image styles information'),
    ];

    foreach ($breakpointConfiguration as $imageStyleId => $imageStyle) {
      $template[$imageStyleId] = [
        '#theme' => 'responsive_image_field__image_mapping',
        '#multiplier' => $imageStyle['multiplier'],
        '#type' => $imageStyle['image_mapping_type'],
        '#sizes' => $imageStyle['image_mapping']['sizes'] ?? [],
        '#image_styles' => $imageStyle['image_mapping']['sizes_image_styles'] ?? [],
        '#effects' => $imageStyle['image_mapping_type'] == 'image_style' ? $this->imageStylesEffects[$imageStyle['image_mapping']] : $this->imageStylesEffects,
      ];
    }
    return $template;
  }

}
